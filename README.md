# Run Docker-compose

`docker-compose up --build -d` 

# Test API Model
```
http://localhost/test : POST
NameKey : image
Body :  add file image 
```
# API Insert Data FistInfo In Mongo 
```
http://localhost/fish : POST
```
```
Body Json :
{
    "breed": "ชื่อสายพันธุ์ปลา",
    "fishName": "ชื่อปลา",
    "note":"รายละเอียดของปลา"
}
```
# API Get Fist In Mongo 
```
http://localhost/fishby/{id} : GET
param {id} <<<< ตัวนี้จะเอา ID มาจาก ใน mongo >>>> ใส่ไปที่ URL และ ยิงได้เลย
```
```
Return ออกมาเป็น Json Document 
{
    "breed": "ชื่อสายพันธุ์ปลา",
    "fishName": "ชื่อปลา",
    "note":"รายละเอียดของปลา"
}
```

# Use Train Model Install Package
```
use python 3.6.5
pip install keras 
pip install tensorflowjs
pip install matplotlib
``` 

# Path Folder
```
         train > ปลาชนิดที่หนึ่ง 
               > ปลาชนิดที่สอง
    validation > ปลาชนิดที่หนึ่ง 
               > ปลาชนิดที่สอง
          test > test.png
```